package com.pierreduchemin.soupoftheday.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import com.pierreduchemin.soupoftheday.R
import com.pierreduchemin.soupoftheday.data.network.MenuServiceFactory
import com.pierreduchemin.soupoftheday.data.network.model.Lang
import com.pierreduchemin.soupoftheday.data.network.model.MenuPage
import com.tapadoo.alerter.Alerter
import kotlinx.android.synthetic.main.activity_soup_list.*
import kotlinx.android.synthetic.main.content_soup_list.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class SoupListActivity : AppCompatActivity() {

    private val aTAG = SoupListActivity::class.java.simpleName
    private val menuAdapter = MenuAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_soup_list)
        setSupportActionBar(toolbar)

        rvMenus.layoutManager = LinearLayoutManager(baseContext)
        rvMenus.adapter = menuAdapter

        displayLoading(true)

        MenuServiceFactory.getMenuService()
                ?.getMenuPage(Lang.getFromDeviceLang(Locale.getDefault().language))
                ?.enqueue(object : Callback<MenuPage> {
                    override fun onResponse(call: Call<MenuPage>?, response: Response<MenuPage>?) {

                        displayLoading(false)

                        if (response != null && response.isSuccessful) {
                            val body = response.body()
                            if (body == null) {
                                displayError()
                                return
                            }
                            menuAdapter.addWeekMenu(body.weekMenus)
                        } else {
                            displayError()
                            Log.d(aTAG, response?.errorBody().toString())
                        }
                    }

                    override fun onFailure(call: Call<MenuPage>?, t: Throwable?) {
                        displayLoading(false)
                        displayError()
                    }
                })
    }

    fun displayLoading(isLoading : Boolean) {
        spinner.visibility = if (isLoading) View.VISIBLE else View.GONE
    }

    fun displayError() {
        Alerter.create(this@SoupListActivity)
                .setTitle("Error")
                .setText("Not able to load page")
                .enableInfiniteDuration(true)
                .setBackgroundColorRes(R.color.colorError)
                .show()
    }
}
