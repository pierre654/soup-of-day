package com.pierreduchemin.soupoftheday.data.network

import android.util.Log

import com.pierreduchemin.soupoftheday.BuildConfig

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import pl.droidsonroids.retrofit2.JspoonConverterFactory
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory


object MenuServiceFactory {

    private val TAG = MenuServiceFactory::class.java.simpleName

    private val BASE_URL = "http://www.soupson.ca/"

    private var menuService: MenuService? = null

    private val loggingInterceptor: Interceptor
        get() {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            return logging
        }

    fun getMenuService(): MenuService? {
        if (menuService == null) {
            menuService = getRetrofit(BASE_URL)
                    .create(MenuService::class.java)
        }
        return menuService
    }

    private fun getRetrofit(baseUrl: String?): Retrofit {
        if (baseUrl == null || baseUrl.trim { it <= ' ' }.isEmpty()) {
            throw IllegalArgumentException("Invalid baseUrl")
        }//                || !NetworkUtils.isUrl(baseUrl)

        val builder = OkHttpClient.Builder()

        if (BuildConfig.DEBUG) {
            Log.w(TAG, "getRetrofit(): Retrofit Log activated")
            builder.addInterceptor(loggingInterceptor)
        }

        return Retrofit.Builder()
                .client(builder.build())
                .baseUrl(baseUrl)
                .addConverterFactory(JspoonConverterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }
}